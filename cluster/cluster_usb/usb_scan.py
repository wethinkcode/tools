#!/usr/bin/python

# Arno van Wyk 30/03/2017
# Connects through ssh to all the cluster computers listed in list.machine_ssh

import subprocess
import sys
from subprocess import PIPE, Popen

def ssh():
	with open('list.machine_ssh') as f:
	    cluster_list = f.readlines()
	cluster_list = [x.strip() for x in cluster_list]

	# Lists USB devices
	COMMAND="ioreg -p IOUSB | sed 's/[^o]*o //; s/  <.*$//' | grep -v '^Root.*'"

	for host in cluster_list:
		print host
		ssh = subprocess.Popen(["ssh", host, COMMAND],
					shell=False,
					stdout=subprocess.PIPE,
					stderr=subprocess.PIPE)
		result = ssh.stdout.readlines()
		if result == []:
			error = ssh.stderr.readlines()
			print >>sys.stderr, "ERROR: %s" % error
		else:
			usb_scan(result)

def usb_scan(result):
	mouse_present = 0
	keyboard_present = 0
	devices = []
	hub_id = 0
	for device in result:
		device = device.lower().rstrip()
		if device.find("keyboard hub") > -1:
			hub_id = int(''.join([i for i in device if i.isdigit()]))
		elif device.find("keyboard") > -1:
			keyboard_present = 1
		elif device.find("mouse") > -1:
			mouse_present = 1
		elif (device.lower().find("brcm20702") > -1):
			continue
		elif (device.lower().find("bluetooth usb host controller") > -1):
			continue
		elif (device.lower().find("facetime hd camera") > -1):
			continue
		else:
			devices.append(device)
	if keyboard_present == 0:
		print "\033[0;31mNo keyboard detected\x1b[0m"
	elif mouse_present == 0:
		print "\033[0;31mNo mouse detected\x1b[0m"
	if devices != [] and hub_id != 0:
		verify_usb(hub_id, devices)

def verify_usb(hub_id, devices):
	for device in devices:
		dev_id = int(''.join([i for i in device if i.isdigit()]))
		device = ''.join([i for i in device if not i.isdigit()])
		if dev_id - hub_id >= 10000 and dev_id - hub_id <= 90000:
			print '\x1b[6;30;42m' + device + ' Connected to keyboard hub!\x1b[0m'
		else:
			print '\x1b[5;30;41m' + device + ' Connected to back of iMac!\x1b[0m'

ssh()
