#!/usr/bin/python

# Arno van Wyk 30/03/2017
# Adds all the cluster computers listed in list.machine_hosts
# to the .ssh/known_hosts file

import subprocess
import sys

with open('list.machine_hosts') as f:
    cluster_list = f.readlines()
cluster_list = [x.strip() for x in cluster_list]

for host in cluster_list:
	print host
	output = subprocess.check_output("ssh-keyscan -H " + host + " >> ~/.ssh/known_hosts", shell=True)
