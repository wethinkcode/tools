#!/bin/bash 

/usr/bin/expect<<EOF

spawn firmwarepasswd -setpasswd
expect {
	"Enter password:" {
		send "toto\r"
		expect "Enter new password:"
		send "toto\r"
		expect "Re-enter new password:"
		send "toto\r"
	}
	"Enter new password:" {
		send "toto\r"
		expect "Re-enter new password:"
		send "toto\r"
	}
}
expect eof
EOF
