#!/usr/bin/python

# Arno van Wyk 06/07/2017
# Strips hostnames and IP addresses from cluster-4.conf and writes them to a CSV file

import sys

# read the list file and create an array

with open('cluster-4.conf') as f:
    cluster_list = f.readlines()
cluster_list = [x.strip() for x in cluster_list]
file = open('output.csv', 'w' );
for host in cluster_list:
	if (host.find("host") > -1):

		# removes the first 5 characters and the last 2 from the current string
		
		host = host[5:len(host)-2]
		file.write(host + ",");
	elif (host.find("fixed-address") > -1):
		
		# removes the first 14 characters and the last one from the current string

		host = host[14:len(host)-1]
		file.write(host + "\n");

