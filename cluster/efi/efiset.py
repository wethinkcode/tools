#!/usr/bin/python

# Arno van Wyk 06/07/2017
# Sets the EFI passwords on all the iMacs in the list.csv file
# Must be run from ansible

import sys
import time
import subprocess
from subprocess import PIPE, Popen, call

# read the csv file and create an array

with open('list.csv') as f:
	cluster_list = f.readlines()
	cluster_list = [x.strip() for x in cluster_list]

# creates host and password pair
# eg. e5r10p11 HwEn2Bsh5qzBHDbJ

pair = []
for host in cluster_list:
	# open script file
	file_log = open('output.log', 'a' );
	file_log.write("\n")
	file0 = open('firmware.sh', 'w' );
	if (host.find("POSTE") > -1) or host == "":
		continue
	host = host.split(",")

	pair.append(host[0]);
	pair.append(host[11]);
	print pair;

	# build check script
	file0.write("#!/bin/bash\n")
	file0.write("/usr/bin/expect <<EOF\n")
	file0.write("spawn firmwarepasswd -setpasswd\n")
	file0.write("expect {\n")

	file0.write("\"Enter password:\" {\n")
	file0.write("exit 1\n")
	file0.write("}\n")

	file0.write("\"Enter new password:\" {\n")
	file0.write("send \"" + pair[1] +"\\r\"\n")
	file0.write("exp_continue\n")
	file0.write("}\n")
	
	file0.write("\"Re-enter new password:\" {\n")
	file0.write("send \"" + pair[1] +"\\r\"\n")
	file0.write("}\n")
	

	file0.write("}\n")
	file0.write("expect eof\n")
	file0.write("EOF\n")
	file0.write("rm firmware.sh\n")
	file0.write("sleep 5\n")
	file0.write("reboot")

	file0.close()

# scp script

	cmd = "scp firmware.sh " + pair[0] + ":/tmp/."
	scp = call(cmd.split())
	if (scp != 0):
		print ("scp failed!\n")
		del pair[:]
		continue

# ssh & execute script

	COMMAND="sh /tmp/firmware.sh"

	ssh = subprocess.Popen(["ssh", pair[0], COMMAND],
				shell=False,
				stdout=subprocess.PIPE,
				stderr=subprocess.PIPE)
	result = ssh.stdout.readlines()
	if result == []:
		error = ssh.stderr.readlines()
		print >>sys.stderr, "ERROR: %s" % error
		for message in error:
			file_log.write(time.strftime("%d/%m/%Y %I:%M:%S ") + pair[0] + " " + message)
	else:
		for message in result:
			file_log.write(time.strftime("%d/%m/%Y %I:%M:%S ") + pair[0] + " " + message)
	file_log.write("\n-----------------------\n")

#clear pair to prepare for next host
	del pair[:]
