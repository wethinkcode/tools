#!/usr/bin/python

# Arno van Wyk 06/07/2017
# Creates a host list from cluster-4.conf

import sys

# read the list file and create an array

with open('cluster-4.conf') as f:
	cluster4_list = f.readlines()
	cluster4_list = [x.strip() for x in cluster4_list]

with open('cluster-5.conf') as f:
	cluster5_list = f.readlines()
	cluster5_list = [x.strip() for x in cluster5_list]

file0 = open('list.machine_hosts', 'w' );
file1 = open('list.machine_ssh', 'w' );

# creates host list
# eg. e5r10p11.wtc.co.za

for host in cluster4_list:
	if (host.find("host") > -1):

		# removes the first 5 characters and the last 2 from the current string
		
		host = host[5:len(host)-2]
		file0.write(host + ".wtc.co.za\n");

for host in cluster5_list:
	if (host.find("host") > -1):

		# removes the first 5 characters and the last 2 from the current string
		
		host = host[5:len(host)-2]
		file0.write(host + ".wtc.co.za\n");


# create ssh host list
# eg. root@e5r10p11.wtc.co.za

for host in cluster4_list:
	if (host.find("host") > -1):

		# removes the first 5 characters and the last 2 from the current string
		
		host = host[5:len(host)-2]
		file1.write("root@" + host + ".wtc.co.za\n");

for host in cluster5_list:
	if (host.find("host") > -1):

		# removes the first 5 characters and the last 2 from the current string
		
		host = host[5:len(host)-2]
		file1.write("root@" + host + ".wtc.co.za\n");
