#!/bin/sh
# Simulates hitting a key on OS X
# http://apple.stackexchange.com/a/63899/72339

echo "tell application \"System Events\" to keystroke \"exam\"" | osascript
echo "tell application \"System Events\" to keystroke return" | osascript
echo "tell application \"System Events\" to keystroke \"exam\"" | osascript
echo "tell application \"System Events\" to keystroke return" | osascript
