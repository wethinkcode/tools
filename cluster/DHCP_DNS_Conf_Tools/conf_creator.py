#!/usr/bin/python
## Written by arnovan-
## 05/08/2017

print "Please enter floor number:"
floor = str(input())
with open('./configs/input/cluster-' + floor + '.conf') as lst:
	input_list = lst.readlines()
input_list = [x.strip() for x in input_list]

index = 1
imac_num = ""
row_num = ""
dhcp_output = open('./configs/output/cluster-' + floor + '.conf','w')
dns_output = open('./configs/output/db.wethinkcode.co.za_' + floor + 'th','w')
revdns_output = open('./configs/output/rev.20' + floor + '.db.wethinkcode.co.za','w')

for line in input_list:
	# get row and imac number
	if (line.find("host") > -1):	
		dhcp_output.write(line)
		parse = line[:-2]	
		head, sep, tail = parse.partition('p')
		imac_num = tail	
		head, sep, tail = head.partition('r')
		row_num = tail

################ writes to dhcp config file ################
#	if (line.find("host") > -1):
#		dhcp_output.write(line)
	elif (line.find("hardware") > -1):
		dhcp_output.write("\t" + line)
	elif (line.find("fixed-address") > -1):
		dhcp_output.write("\tfixed-address 10.20" + floor + "." + row_num + "." + imac_num + ";")
	elif (line.find("}") > -1):
		dhcp_output.write(line)

		################## writes to dns db file ###################
		dns_output.write("e"+ floor +"r" + row_num + "p" + imac_num + "\t\t\tIN\tA\t10.20" + 
							floor + "." + row_num + "." + imac_num + "\n")

		################ writes to rev dns db file #################
		revdns_output.write(str(index) + "\t\tPTR\te" + floor + "r" + row_num + "p" + imac_num + 
				".wethinkcode.co.za. ; 10.20" + floor + "." + row_num + "." + imac_num + "\n")
		index = index + 1
	dhcp_output.write("\n")
