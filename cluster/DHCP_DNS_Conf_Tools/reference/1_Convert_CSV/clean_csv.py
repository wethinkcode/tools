#!/usr/bin/python
## Written by arnovan- 15/04/2017
## Cleans and converts a csv of iMac mac addresses in the format of "Example_Row_4.csv"
## into a clean list for the DHCP_Conf_Creator tool. Each row represents a row of iMacs and
## each column represents an iMac counted from the left.
## eg. Top right mac address is for e4r1p1
## Empty lines are counted as a missing iMac for a reason
## good luck

with open('Example_Row_4.csv')as f:
	imac_list = f.readlines()
imac_list = [x.strip() for x in imac_list]

neat_list = []
for imac in imac_list:
	imac = imac.lower()
	neat_list.append(imac.split(','))

f = open('address_list', 'w')
for row in neat_list:
	for imac in row:
		f.write("%s\n" % (imac))
f.close()
