#!/usr/bin/python
## Written by arnovan- uses the mac address list to create dns lookup list
## It seems unnecessary but it's a quick retrofitted script
## good luck

print "Please enter the floor number:"
floor = input()
print "Please enter the maximum number of iMacs per row:"
imacs_per_row = input()
print "Please enter the number of rows:"
rows = input()
ip = 1
index = 0

with open('address_list') as lst:
	address_list = lst.readlines()
address_list = [x.strip() for x in address_list]

f = open('rev.204.db.wtc.co.za','w')

for x in range (1, rows + 1):
    for y in range (1, imacs_per_row + 1):
	if (address_list[index]) != '':
		f.write("%s\t\tPTR\te%sr%sp%s.wtc.co.za. ; 10.204.0.%s\n" % (ip, floor, x, y, ip))
		ip += 1
	index += 1

f.close() # you can omit in most cases as the destructor will call it
