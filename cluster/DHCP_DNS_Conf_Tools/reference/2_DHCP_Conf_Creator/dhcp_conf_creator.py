#!/usr/bin/python
## Written by arnovan-
## Don't try to edit this, you will burn your eyes...
## good luck

print "Please enter the floor number:"
floor = input()
print "Please enter the maximum number of iMacs per row:"
imacs_per_row = input()
print "Please enter the number of rows:"
rows = input()
ip = 1
index = 0

with open('address_list') as lst:
	address_list = lst.readlines()
address_list = [x.strip() for x in address_list]

f = open('cluster-4.conf','w')

for x in range (1, rows + 1):
    for y in range (1, imacs_per_row + 1):
	if (address_list[index]) != '':
		f.write("host e%sr%sp%s {\n" % (floor, x, y))
		f.write("\thardware ethernet %s;\n" % (address_list[index]))
		f.write("\tfixed-address 10.204.0.%s;\n}\n" % (ip))
		ip += 1
	index += 1

f.close() # you can omit in most cases as the destructor will call it
