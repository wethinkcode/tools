#!/usr/bin/python

import sys
import time
import subprocess
from subprocess import PIPE, Popen, call
import string
import random

cohort = "2016"
#cohort = "2017"

with open('uid_list.db') as f:
	uid_list = f.readlines()
	uid_list = [x.strip() for x in uid_list]

'''
############## Get current uid #############
with open('ldap_fetch.db') as f:
	ldap_parse = f.readlines()
	ldap_parse = [x.strip() for x in ldap_parse]
uid_list = []
for info in ldap_parse:
	if (info.find("uidNumber:") > -1):
		info = info.split(" ")
		uid_list.append(info[1])
uid_current = int(max(uid_list)) + 1
print uid_current
##############################################
'''

############### Read csv ####################
with open(cohort + '.csv') as f:
	student_list = f.readlines()
	student_list = [x.strip() for x in student_list]

#file_log = open('output.log', 'a');


ldif = open('new_students_' + cohort + '.ldif', 'w');
ldif_mod = open('student_pics_' + cohort + '.ldif', 'w');
krb = open('students_pass_' + cohort + '.sh', 'w');

for info in student_list:	
	uid_number = "NULL"
	if (info.find("First Name") > -1):
		continue
	info = info.split(",")
	#print info

	firstname = info[0]
	lastname = info[1].upper()
	username = info[2].split("@")[0]
	email = info[2]
	password = info[3]
	if (info[7] == ""):
		mobile = "42"
	else:
		mobile = info[7]

	for number in uid_list:
 		if (number.find(username) > -1):
                	number = number.split(" ")
                	uid_number = number[1]
	#	else:
	#		uid_number = "NULL"
	if (uid_number == "NULL"):
		print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1UID for user: " + username + "not found!\n")

	print ("firstname " + firstname + "")
	print ("lastname " + lastname + "")
	print ("username " + username + "")
	print ("email " + email + "")
	print ("password " + password + "")
	print ("mobile " + mobile + "")
	print ("uidnumber " + uid_number + "")

	ldif.write("dn: uid=" + username + ",ou=" + cohort + ",ou=johannesburg,ou=people,dc=wethinkcode,dc=co,dc=za\n")
	ldif.write("cn: " + firstname + " " + lastname + "\n")
	ldif.write("givenName: " + firstname + "\n")
	ldif.write("sn: " + lastname + "\n")
	ldif.write("uid: " + username + "\n")
	ldif.write("uidNumber: " + uid_number + "\n")
	ldif.write("gidNumber: 5000\n")
	ldif.write("mail: " + email + "\n")
	ldif.write("mobile: +" + mobile + "\n")
	ldif.write("loginShell: /bin/zsh\n")
	ldif.write("objectClass: posixAccount\n")
	ldif.write("objectClass: shadowAccount\n")
	ldif.write("objectClass: apple-user\n")
	ldif.write("objectClass: inetOrgPerson\n")
	ldif.write("homeDirectory: /goinfre/" + username + "\n")
	ldif.write("\n")

	ldif_mod.write("dn: uid=" + username + ",ou=" + cohort + ",ou=johannesburg,ou=people,dc=wethinkcode,dc=co,dc=za\n")
	ldif_mod.write("changetype: modify\n")
	ldif_mod.write("add: jpegPhoto\n")
	ldif_mod.write("jpegPhoto:< file:///images/"+ cohort + "/small_" + username + ".jpg\n")
	ldif_mod.write("\n")
	
#	uid_current = "NULL"

	krb.write("kadmin.local -q \"addprinc -pw " + password +
	" -x dn=\"uid=" + username + ",ou=" + cohort +
	",ou=johannesburg,ou=people,dc=wethinkcode,dc=co,dc=za\" " + username + "\"\n")
	krb.write("\n")

ldif.close()
ldif_mod.close()
krb.close()

'''
cmd = "scp -P 4222 new_student.ldif root@ldap.wethinkcode.co.za:~/"
scp = call(cmd.split())
if (scp != 0):
	print ("scp failed!\n")
	continue


#ssh ldap
COMMAND="ldapadd -x -w secret -D \"cn=admin,dc=wethinkcode,dc=co,dc=za\" -f \"new_student.ldif\""
HOST_LDAP= "root@ldap-wethinkcode.wethinkcode.co.za"

ssh = subprocess.Popen(["ssh", "-p4222", HOST_LDAP, COMMAND],
				shell=False,
				stdout=subprocess.PIPE,
				stderr=subprocess.PIPE)
result = ssh.stdout.readlines()
if result == []:
	error = ssh.stderr.readlines()
	print >>sys.stderr, "ERROR: %s" % error
	for message in error:
		file_log.write(time.strftime("%d/%m/%Y %I:%M:%S ") + message)
else:
	for message in result:
		file_log.write(time.strftime("%d/%m/%Y %I:%M:%S ") + message)


#ssf kdc

	COMMAND="kadmin.local -q \"addprinc -pw " + password + " " + username + "\""
	print COMMAND
HOST_KDC= "root@kdc-wethinkcode.wethinkcode.co.za"
        
ssh = subprocess.Popen(["ssh", "-p4222", HOST_KDC, COMMAND],
                                shell=False,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
result = ssh.stdout.readlines()
if result == []:
	error = ssh.stderr.readlines()
	print >>sys.stderr, "ERROR: %s" % error
	for message in error:
		file_log.write(time.strftime("%d/%m/%Y %I:%M:%S ") + message)
else:
	for message in result:
		file_log.write(time.strftime("%d/%m/%Y %I:%M:%S ") + message)
'''
