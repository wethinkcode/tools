#!/usr/bin/python

import sys
import time
import subprocess
from subprocess import PIPE, Popen, call
import string
import random

def pass_generator(size=8, chars=string.ascii_uppercase + string.digits):
	return ''.join(random.choice(chars) for _ in range(size))


############## Get current uid #############
with open('ldap_fetch.db') as f:
	ldap_parse = f.readlines()
	ldap_parse = [x.strip() for x in ldap_parse]

uid_list = []
for info in ldap_parse:
	if (info.find("uidNumber:") > -1):
		info = info.split(" ")
		uid_list.append(info[1])
uid_current = int(max(uid_list)) + 1
print uid_current
##############################################

############### Read csv ####################
with open('students2017.csv') as f:
	student_list = f.readlines()
	student_list = [x.strip() for x in student_list]

file_log = open('output.log', 'a');
passwd = open('student_passwords.csv', 'a');

for info in student_list:
	info = info.split(",")
	
	#print info

	firstname = info[7]
	lastname = info[8]
	username = info[0]
	cohort = "2017"
	email = username + "@student.42.fr"
	password = pass_generator()
	mobile = "42"
	uid_number = str(uid_current)

	passwd.write(username + "," + password + "\n")

	print ("firstname " + firstname + "\n")
	print ("lastname " + lastname + "\n")
	print ("username " + username + "\n")
	print ("email " + email + "\n")
	print ("password " + password + "\n")
	print ("mobile " + mobile + "\n")
	print ("uidnumber " + uid_number + "\n")


#############################################

	ldif = open('new_student.ldif', 'w');
	ldif.write("dn: uid=" + username + ",ou=" + cohort + ",ou=johannesburg,ou=people,dc=wtc,dc=co,dc=za\n")
	ldif.write("cn: " + firstname + " " + lastname + "\n")
	ldif.write("givenName: " + firstname + "\n")
	ldif.write("sn: " + lastname + "\n")
	ldif.write("uid: " + username + "\n")
	ldif.write("uidNumber: " + uid_number + "\n")
	ldif.write("gidNumber: 5000\n")
	ldif.write("mail: " + email + "\n")
	ldif.write("mobile: +" + mobile + "\n")
	ldif.write("loginShell: /bin/zsh\n")
	ldif.write("objectClass: posixAccount\n")
	ldif.write("objectClass: shadowAccount\n")
	ldif.write("objectClass: apple-user\n")
	ldif.write("objectClass: inetOrgPerson\n")
	ldif.write("homeDirectory: /goinfre/" + username + "\n")
	ldif.close()

	cmd = "scp -P 4222 new_student.ldif root@ldap.wethinkcode.co.za:~/"
	scp = call(cmd.split())
	if (scp != 0):
		print ("scp failed!\n")
		continue

	uid_current = uid_current + 1

	#ssh ldap
	COMMAND="ldapadd -x -w secret -D \"cn=admin,dc=wtc,dc=co,dc=za\" -f \"new_student.ldif\""
	HOST_LDAP= "root@ldap.wethinkcode.co.za"

	ssh = subprocess.Popen(["ssh", "-p4222", HOST_LDAP, COMMAND],
				shell=False,
				stdout=subprocess.PIPE,
				stderr=subprocess.PIPE)
	result = ssh.stdout.readlines()
	if result == []:
		error = ssh.stderr.readlines()
		print >>sys.stderr, "ERROR: %s" % error
		for message in error:
			file_log.write(time.strftime("%d/%m/%Y %I:%M:%S ") + message)
	else:
		for message in result:
			file_log.write(time.strftime("%d/%m/%Y %I:%M:%S ") + message)


	#ssf kdc

        COMMAND="kadmin.local -q \"addprinc -pw " + password + " " + username + "\""
	print COMMAND
	HOST_KDC= "root@kdc.wethinkcode.co.za"
        
	ssh = subprocess.Popen(["ssh", "-p4222", HOST_KDC, COMMAND],
                                shell=False,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        result = ssh.stdout.readlines()
        if result == []:
                error = ssh.stderr.readlines()
                print >>sys.stderr, "ERROR: %s" % error
                for message in error:
                        file_log.write(time.strftime("%d/%m/%Y %I:%M:%S ") + message)
        else:
                for message in result:
                        file_log.write(time.strftime("%d/%m/%Y %I:%M:%S ") + message)
