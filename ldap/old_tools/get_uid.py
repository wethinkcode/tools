#!/usr/bin/python

# Arno van Wyk 06/07/2017
# Creates a host list from cluster-4.conf

import sys
import time
import subprocess
from subprocess import PIPE, Popen, call

COMMAND = "ldapsearch -z 0 -w secret -b \"dc=wethinkcode,dc=co,dc=za\" -D \"cn=admin,dc=wethinkcode,dc=co,dc=za\""
HOST = "root@10.242.0.118"

db = open('ldap_fetch.db', 'w');
log = open('error.log', 'a');

ssh = subprocess.Popen(["ssh", "-p4222", HOST, COMMAND],
			shell=False,
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE)
result = ssh.stdout.readlines()
if result == []:
	error = ssh.stderr.readlines()
	print >>sys.stderr, "ERROR: %s" % error
	for message in error:
		log.write(time.strftime("%d/%m/%Y %I:%M:%S ") + message)
else:
	for message in result:
		db.write(message)

with open('ldap_fetch.db') as f:
	ldap_parse = f.readlines()
	ldap_parse = [x.strip() for x in ldap_parse]

uid_list = []
for info in ldap_parse:
	if (info.find("uidNumber:") > -1):
		info = info.split(" ")
		uid_list.append(info[1])
uid_current = int(max(uid_list)) + 1
print "The next available uid is: " + str(uid_current)
