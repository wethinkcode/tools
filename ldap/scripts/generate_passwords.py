#!/usr/bin/python

import sys
import string
import random


number = raw_input('Enter the number of students: ')

def random_id(length):
    number = '0123456789'
    alpha = 'abcdefghijklmnopqrstuvwxyz'
    id = ''
    for i in range(0,length,2):
        id += random.choice(number)
        id += random.choice(alpha)
    return id


entry = open('random_passwords.csv', 'w');

for x in range(int(number)):	
	rand_pass = random_id(8) 
	entry.write(rand_pass + "\n")
	print rand_pass


entry.close()
