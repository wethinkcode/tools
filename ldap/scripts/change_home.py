#!/usr/bin/python

# Arno van Wyk 05/12/2017
# Script to change user home on the LDAP

import datetime
import subprocess
import os

log = open('operation.log', 'a')
if not os.path.exists('/tmp/user-dash'):
    os.makedirs('/tmp/user-dash')

def gettime():
    date = datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S")
    return date

def main():
    option = ''
    while option not in {'0', '1', '2'}:
        os.system('clear')
        print '0 - Change single user\'s home'
        print '1 - Change home directory of multiple accounts (From a CSV)'
        print '2 - Quit'
        option = raw_input('Make a selection: ')
    if option == '0':
        username = raw_input('Enter a username: ')
        user = scan_ldap(username)
        if user == 'error1':
            return 1
        elif user == 'error2':
            raw_input('User not found! Press Enter to continue...')
            main();return 1
        home_dir = raw_input('Enter new home location (eg. /goinfre/username): ')
        load_ldif(user, home_dir)
    elif option == '1':
        csv = select_csv()
        if csv == 'error0':
            raw_input('No CSV files available! Press Enter to continue...')
            main();return 1
        elif csv == 'error1':
            raw_input('Invalid selection! Press Enter to continue...')
            main();return 1
        print "Selected: " + csv
        status = read_csv(csv)
        if status == 'error0':
            raw_input('Operation cancelled by user! Press Enter to continue...')
            main();return 1
        if status == 'error1':
            raw_input("LDAP error! Press Enter to continue...")
            main();return 1
        if status == 0:
            raw_input('\n\nOperation completed successfully! Press Enter to exit...')
    elif option == '2':
        return 0

def scan_ldap(username):
    proc = subprocess.Popen(['ldapsearch', '-z', '0', '-b', 'dc=wethinkcode,dc=co,dc=za', '-D',\
                'cn=admin,dc=wethinkcode,dc=co,dc=za', '-w', 'secret'], stdout=subprocess.PIPE)
    db = proc.stdout.readlines()
    if db[0].find('# extended LDIF') >= 0:
        print "Connected to LDAP"
    else:
        os.system('clear')
        print "LDAP Error"
        log.write( gettime() + ' Error when accessing LDAP\n')
        return 'error1'
    for line in db:
        if line != '':
            if line.find('uid=' + username + ',') >= 0:
                print 'Located ' + username + '...'
                line = line.split(' ')[1]
                line = line.split(',ou=people')[0]
                line = line + ',ou=people,dc=wethinkcode,dc=co,dc=za'
                return line
    return 'error2'

def select_csv():
    csv_buff = []
    selection = 0
    index = 0
    for file in os.listdir("./"):
        if file.endswith(".csv"):
            csv_buff.append(file)
    if not csv_buff:
        return 'error0'
    os.system("clear")
    print '\n\x1b[6;30;42mSELECT A CSV:\x1b[0m'
    for files in csv_buff:
        print str(index) + ': ' + files
        index = index + 1
    print '\n'        
    selection = raw_input('Select a CSV:')
    if selection.isdigit() == False or int(selection) > len(csv_buff) - 1 or int(selection) < 0:
        os.system("clear")
        print "CSV does not exist!"
        return 'error1'
    else:
        return csv_buff[int(selection)]

def read_csv(csv):
    with open(csv) as f:
        student_list = f.readlines()
        student_list = [x.strip() for x in student_list]
    os.system("clear")
    print ('Filename: ' + csv + '\n')
    for info in student_list:
        print info
    print '\n\n--------------------------------------------------------\nThese accounts will be edited.'
    print '\n\n\033[93mPlease make sure the CSV follows the follwing structure:\nUser1\nUser2\nUser3'
    validate = raw_input('\n\nIs this info correct? Enter "y" to continue, anything else to cancel\x1b[0m ')
    if validate.lower() != 'y':
        return 'error0'
   # home_dir = raw_input('Enter new home location (eg. /goinfre/username): ')
    validate = raw_input\
            ('\n\033[31mWARNING! This could damage your LDAP and KDC servers. Press enter to continue, "n" to cancel\x1b[0m ')
    if validate.lower() == 'n' or validate.lower() == 'no':
        return 'error0'
    for username in student_list:
        if username == '':
            continue
        dn = scan_ldap(username)
        if dn == 'error1':
            return 'error1'
        if dn == 'error2':
            raw_input('Username ' + username + ' not found in the LDAP. Press enter to continue...')
            continue
        home_dir = '/nfs/zfs-student-6/users/' + username
        status = load_ldif(dn, home_dir)
        if status != 0:
            return 'error1'
        print 'Successfully modified \"' + dn + '\"\n\n'
        print '--------------------------------------\n'
        log.write( gettime() + ' Modified homeDirectory attribute at \"' + dn + '\" to \"' + home_dir + '\"\n')
    return 0

def load_ldif(dn, home_dir):
    ldif = open('/tmp/user-dash/set_home.ldif', 'w')
    ldif.write('dn: ' + dn + '\n')
    ldif.write('changetype: modify\n')
    ldif.write('replace: homeDirectory\n')
    ldif.write('homeDirectory: ' + home_dir + '\n')
    ldif.close()
    proc = subprocess.Popen(['cat', '/tmp/user-dash/set_home.ldif'], stdout=subprocess.PIPE)
    ldif_test = proc.stdout.readlines()
    if ldif_test[0].find('dn') >= 0:
        print "LDIF file created successfully..."
    else:
        print '\033[31mError when creating LDIF file\x1b[0m'
        return 1
    print 'Changing account home directory on the LDAP...'
    proc = subprocess.call(['ldapadd', '-x', '-w', 'secret', '-D', 'cn=admin,dc=wethinkcode,dc=co,dc=za', '-f', '/tmp/user-dash/set_home.ldif'])
    if proc != 0:
        print '\033[31mError when changing home for: ' + dn + ' \x1b[0m' 
        return 1
    else:
        return 0

main()
log.write('\n')
log.close()
