#!/usr/bin/python

# Arno van Wyk 04/11/2017
# LDAP db parser

import subprocess
import os
import time

#global next_uid
if not os.path.exists('/tmp/user-dash'):
    os.makedirs('/tmp/user-dash')



#################################################################################################
def main():
    '''
    next_uid = next_uidNumber()
    if next_uid == 1:
        print "Error: Could not fetch uidNumber from LDAP"
        return 1
    print next_uid
    '''
    csv = select_csv()
    if csv == 'error0':
        print "Error: No CSV files available"
        return 1
    elif csv == 'error1':
        print "Error: Invalid selection"
        return 1
    elif csv == 'error2':
        print "Error: Invalid selection"
        return 1
    print "Selected: " + csv
    status = read_csv(csv)
    if status == 'error0':
        print "Operation cancelled by user"
        return 1
    if status == 'error1':
        print "Error: The campus or cohort you entered does not exist in the LDAP"
        raw_input("Press Enter to continue...")
        main()
    if status == 'error2':
        print "Error: LDAP account creation failed"
        return 1
    if status == 'error3':
        print "Error: Kerberos principal creation failed"
        return 1
#################################################################################################




#################################################################################################

def scan_ldap(cohort, campus, is_bootcamp):
    valid = 0
    buff_uid = []
    proc = subprocess.Popen(['ldapsearch', '-z', '0', '-b', 'dc=wethinkcode,dc=co,dc=za', '-D',\
                'cn=admin,dc=wethinkcode,dc=co,dc=za', '-w', 'secret'], stdout=subprocess.PIPE)
    db = proc.stdout.readlines()
    if db[0].find('# extended LDIF') >= 0:
        print "Connected to LDAP"
    else:
        os.system('clear')
        print "LDAP Error"
        return 1
    for line in db:
        if line != '':
            if line.find('uidNumber:') >= 0:
                buff_uid.append(int(line.split(' ')[1]))
            if is_bootcamp == 1 and line.find('ou=' + cohort + ',ou=bootcamp,ou=' + campus) >= 0:
                valid = 1
            elif line.find('ou=' + cohort + ',ou=' + campus) >= 0:
                valid = 1
    if valid == 0:
        return 2
    print "Getting next available uidNumber"
    return int(max(buff_uid)) + 1

#################################################################################################







#################################################################################################

def select_csv():
    csv_buff = []
    selection = 0
    index = 0
    for file in os.listdir("./"):
        if file.endswith(".csv"):
            csv_buff.append(file)
    if not csv_buff:
        return 'error0'
    os.system("clear")
    print '\n\x1b[6;30;42mSELECT A CSV:\x1b[0m'
    for files in csv_buff:
        print str(index) + ': ' + files
        index = index + 1
    print '\n'        
    selection = raw_input('Select a CSV:')
    if selection.isdigit() == False or int(selection) > len(csv_buff) - 1 or int(selection) < 0:
        os.system("clear")
        print "CSV does not exist!"
        return 'error1'
    else:
        return csv_buff[int(selection)]

#################################################################################################






#################################################################################################
def read_csv(csv):
    cohort = ''
    campus = ''
    is_bootcamp = 0
    with open(csv) as f:
        student_list = f.readlines()
        student_list = [x.strip() for x in student_list]
    os.system("clear")
    while campus == '':
        campus = raw_input('\nPlease enter a campus (eg: Johannesburg): ')
    campus = campus.lower()
    validate = raw_input('\nIs this a bootcamp? Enter "y" for yes, anything else for no: ')
    if validate.lower() == 'y' or validate.lower() == 'yes':
        is_bootcamp = 1
    if is_bootcamp == 1:
        while cohort not in {'january', 'february', 'march', 'april', 'may', 'june', 'july',\
                'august', 'september', 'october', 'november', 'december'}:
            if cohort != '':
                print 'The date you entered is not valid!'
            cohort = raw_input('\nEnter bootcamp month (eg. October): ')
            cohort = cohort.lower()
    else:
        while cohort == '':
            cohort = raw_input('\nEnter cohort year (eg. 2018): ')
    
    os.system("clear")
    print ('Filename: ' + csv + '\n')
    for info in student_list:
        print info
    if is_bootcamp == 1:
        print '\n\nAccounts will be created for the ' + cohort.title() + ' bootcamp at the ' + campus.title() + ' campus.'
    else:
        print '\n\nAccounts will be created for the ' + cohort.title() + ' cohort at the ' + campus.title() + ' campus.'
    print '\n\n\033[93mPlease make sure the CSV follows the follwing structure:\nFirst Name,Last Name,Email,Username,Password'
    validate = raw_input('\n\nIs this info correct? Enter "y" to continue, anything else to cancel\x1b[0m ')
    if validate.lower() != 'y':
        os.system("clear")
        return 'error0'
    validate = raw_input\
            ('\n\033[31mWARNING! This could damage your LDAP and KDC servers. Press enter to continue, "n" to cancel\x1b[0m ')
    if validate.lower() == 'n' or validate.lower() == 'no':
        os.system("clear")
        return 'error0'

    next_uid = scan_ldap(cohort, campus, is_bootcamp)
    if next_uid == 1:
        print "Error: Could not fetch uidNumber from LDAP"
        return 1
    if next_uid == 2:
        return 'error1'
    print next_uid
    for info in student_list:
        if (info.lower().find('name') > -1):
            continue
        info = info.split(',')
        firstname = info[0].title()
        lastname = info[1].upper()
        username = info[3].lower()
        password = info[4]

# run ldif function
# log
        status = load_ldif(firstname, lastname, username, password, campus, cohort, is_bootcamp, next_uid)
        if status != 0:
            print "An error occured!"
            return 'error2'
        status = add_princ(password, username, cohort, campus, is_bootcamp)
        if status != 0:
            print "An error occured!"
            return 'error3'
    
        print '\033[92mUser created successfully!\x1b[0m'
        next_uid = next_uid + 1

# run kdc function
# log
# if successful, increment uidnumber

#################################################################################################







#################################################################################################

def load_ldif(firstname, lastname, username, password, campus, cohort, is_bootcamp, uid_number):
    ldif = open('/tmp/user-dash/user-dash.ldif', 'w')
    if is_bootcamp == 1:
	ldif.write("dn: uid=" + username + ",ou=" + cohort + ",ou=bootcamp,ou=" + campus + ",ou=people,dc=wethinkcode,dc=co,dc=za\n")
    else:
	ldif.write("dn: uid=" + username + ",ou=" + cohort + ",ou=" + campus + ",ou=people,dc=wethinkcode,dc=co,dc=za\n")
    ldif.write("cn: " + firstname + " " + lastname + "\n")
    ldif.write("givenName: " + firstname + "\n")
    ldif.write("sn: " + lastname + "\n")
    ldif.write("uid: " + username + "\n")
    ldif.write("uidNumber: " + str(uid_number) + "\n")
    ldif.write("gidNumber: 5000\n")
    ldif.write("mail: " + username + "@student.wethinkcode.co.za\n")
    ldif.write("mobile: 42\n")
    ldif.write("loginShell: /bin/zsh\n")
    ldif.write("objectClass: posixAccount\n")
    ldif.write("objectClass: shadowAccount\n")
    ldif.write("objectClass: apple-user\n")
    ldif.write("objectClass: inetOrgPerson\n")
    ldif.write("homeDirectory: /goinfre/" + username + "\n")
    ldif.close()
    proc = subprocess.Popen(['cat', '/tmp/user-dash/user-dash.ldif'], stdout=subprocess.PIPE)
    ldif_test = proc.stdout.readlines()
    if ldif_test[0].find('dn') >= 0:
        print "LDIF file created successfully..."
    else:
        print '\033[31mError when creating LDIF file\x1b[0m'
        return 1
    print 'Creating user on the LDAP...'
    proc = subprocess.call(['ldapadd', '-x', '-w', 'secret', '-D', 'cn=admin,dc=wethinkcode,dc=co,dc=za', '-f', '/tmp/user-dash/user-dash.ldif'])
    if proc != 0:
        print '\033[31mError when creating LDAP account: dn: uid=' + username + ',ou=' + cohort + ',ou=' + campus + ',ou=people,dc=wethinkcode,dc=co,dc=za \x1b[0m' 
        return 1
    else:
        return 0

#################################################################################################







#################################################################################################

def add_princ(password, username, cohort, campus, is_bootcamp):
    if is_bootcamp == 1:
        os.system('kadmin -p user-dash/admin@WETHINKCODE.CO.ZA -k -t /etc/krb5.keytab -q \"addprinc -pw ' + password + ' -x dn=\"uid=' + username + ',ou=' + cohort + ',ou=bootcamp,ou=' + campus + ',ou=people,dc=wethinkcode,dc=co,dc=za\" ' + username + '\"')
    else:
        os.system('kadmin -p user-dash/admin@WETHINKCODE.CO.ZA -k -t /etc/krb5.keytab -q \"addprinc -pw ' + password + ' -x dn=\"uid=' + username + ',ou=' + cohort + ',ou=' + campus + ',ou=people,dc=wethinkcode,dc=co,dc=za\" ' + username + '\"')

    return 0
#################################################################################################



main()
