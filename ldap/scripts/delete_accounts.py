#!/usr/bin/python

# Arno van Wyk 04/11/2017
# LDAP db parser

import subprocess
import os


#################################################################################################
def main():
    option = ''
    uid = ''
    while option not in {'0', '1', '2', '3'}:
        os.system('clear')
        print '0 - Delete single account'
        print '1 - Delete multiple accounts (From a CSV)'
        print '2 - Change a password'
        print '3 - Quit'
        option = raw_input('Make a selection: ')
    if option == '0':
        username = raw_input('Enter a username: ')
        uid = scan_ldap(username)
    elif option == '1':
        csv = select_csv()
        if csv == 'error0':
            print "Error: No CSV files available"
            return 1
        elif csv == 'error1':
            print "Error: Invalid selection"
            return 1
        elif csv == 'error2':
            print "Error: Invalid selection"
            return 1
        print "Selected: " + csv
        status = read_csv(csv)
        if status != 0:
            print 'Error: Could not delete from the LDAP'
    elif option == '2':
        username = raw_input('Enter a username: ')
    elif option == '3':
        return 0
    if uid == 'error':
        raw_input('Username not found. Press enter to continue...')
        main()
        return 1
    elif uid != 'error' and uid != '':
        while option not in {'y', 'n'}:
            option = raw_input('Are you sure you want to delete username ' + username + '? (y/n) ')
            if option == 'y':
                print 'Deleting user...'
                status = delete_kdc(username)
                status = delete_ldap(uid)
                if status != 0:
                    print 'Error: Could not delete from the LDAP'
            elif option == 'n':
                print 'Operation cancelled by user...\nExiting' ; return 0



def delete_ldap(uid):
    proc = subprocess.call(['ldapdelete', '-x', '-w', 'secret', '-D', 'cn=admin,dc=wethinkcode,dc=co,dc=za', uid])
    if proc != 0:
        print '\033[31mError while deleting LDAP account: ' + uid + ' \x1b[0m' 
        return 1
    else:
        print '\033[31mDeleted LDAP account: ' + uid + ' \x1b[0m' 
        return 0

def delete_kdc(username):
    os.system('kadmin -p user-dash/admin@WETHINKCODE.CO.ZA -k -t /etc/krb5.keytab -q \"delprinc ' + username + '\"')
    return 0

#################################################################################################




#################################################################################################

def scan_ldap(username):
    proc = subprocess.Popen(['ldapsearch', '-z', '0', '-b', 'dc=wethinkcode,dc=co,dc=za', '-D',\
                'cn=admin,dc=wethinkcode,dc=co,dc=za', '-w', 'secret'], stdout=subprocess.PIPE)
    db = proc.stdout.readlines()
    if db[0].find('# extended LDIF') >= 0:
        print "Connected to LDAP"
    else:
        os.system('clear')
        print "LDAP Error"
        return 1
    for line in db:
        if line != '':
            if line.find('uid=' + username + ',') >= 0:
                print 'Located ' + username + '...'
                line = line.split(' ')[1]
                line = line.split(',ou=people')[0]
                line = line + ',ou=people,dc=wethinkcode,dc=co,dc=za'
                print line
                return line
    return 'error'
#################################################################################################



#################################################################################################

def select_csv():
    csv_buff = []
    selection = 0
    index = 0
    for file in os.listdir("./"):
        if file.endswith(".csv"):
            csv_buff.append(file)
    if not csv_buff:
        return 'error0'
    os.system("clear")
    print '\n\x1b[6;30;42mSELECT A CSV:\x1b[0m'
    for files in csv_buff:
        print str(index) + ': ' + files
        index = index + 1
    print '\n'        
    selection = raw_input('Select a CSV:')
    if selection.isdigit() == False or int(selection) > len(csv_buff) - 1 or int(selection) < 0:
        os.system("clear")
        print "CSV does not exist!"
        return 'error1'
    else:
        return csv_buff[int(selection)]

#################################################################################################




#################################################################################################
def read_csv(csv):
    
    with open(csv) as f:
        student_list = f.readlines()
        student_list = [x.strip() for x in student_list]
    os.system("clear")
    print ('Filename: ' + csv + '\n')
    for info in student_list:
        print info
    print '\n\nThese accounts to be deleted.'
    print '\n\n\033[93mPlease make sure the CSV follows the follwing structure:\nFirst Name,Last Name,Email,Username,Password'
    validate = raw_input('\n\nIs this info correct? Enter "y" to continue, anything else to cancel\x1b[0m ')
    if validate.lower() != 'y':
        os.system("clear")
        return 'error0'
    validate = raw_input\
            ('\n\033[31mWARNING! This could damage your LDAP and KDC servers. Press enter to continue, "n" to cancel\x1b[0m ')
    if validate.lower() == 'n' or validate.lower() == 'no':
        os.system("clear")
        return 'error0'
    for info in student_list:
        if (info.lower().find('name') > -1):
            continue
        info = info.split(',')
        print 'username ' + info[3]
        username = info[3].lower()
        uid = scan_ldap(username)
        print username
        print uid
        if uid == 'error':
            raw_input('Username not found. Press enter to continue...')
            continue
        delete_kdc(username)
        status = delete_ldap(uid)
        if status != 0:
            print "An error occured!"
            return 'error2'
        print 'User deleted!'


main()
